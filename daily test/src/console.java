import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import com.gmail.justinslauson.dailytest.dice.dice;
import com.gmail.justinslauson.dailytest.piglatin.*;

public class console {

	public static void main(String[] args) {
		
		boolean doNext = true;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("What is your command?");
		String Input = null;
		
		while(doNext) {
			try {
				Input = br.readLine();
			
				System.out.println(Input);
			
			
				String Command = Input.split(" ")[0];
				
				switch (Command) {
				
					case "exit": doNext = false; break;
					
					case "roll": System.out.println(displayDiceRolls(Input)); break;
			
					default: System.out.println(piglatin.translate(Input.split(" ")));
				}
			
			} catch (IOException e) {
			
				e.printStackTrace();
			}
		}

	}
	
	private static String displayDiceRolls(String args){
		
		String FormattedString = "Dice results: ";
		int[] results = dice.Roll(getRollArgs(args)[0], getRollArgs(args)[1]);
		
		for(int i = 0; i < results.length; i++){
			FormattedString += results[i] + " ";
		}
		
		return FormattedString;
	}
	
	private static int[] getRollArgs(String args) {
		
		int[] intArg = {0,0};
		intArg[0] = Integer.parseInt(args.split(" ")[1]);
		intArg[1] = Integer.parseInt(args.split(" ")[2]);
		return intArg;
		
	}


}
