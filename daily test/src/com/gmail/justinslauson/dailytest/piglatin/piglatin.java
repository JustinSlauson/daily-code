package com.gmail.justinslauson.dailytest.piglatin;

import java.util.Arrays;
import java.util.List;


/*
 * 
The usual rules for changing standard English into Pig Latin are as follows:

For words that begin with consonant sounds, the initial consonant or consonant cluster is moved to the end of the word, and "ay" is added, as in the following examples:

    "happy" → "appyhay"
    "duck" → "uckday"
    "glove" → "oveglay"

For words that begin with vowel sounds or silent letter, "way" is added at the end of the word. Examples are

    "egg" → "eggway"
    "inbox" → "inboxway"
    "eight" → "eightway"

The letter 'y' can play the role of either consonant or vowel, depending on its location

    "yellow" → "ellowyay"
    "rhythm" → "ythmrhay"

In some variants, though, just add an "ay" at the end.

    "egg" → "eggay"

Yet another acceptable variant is to add the ending "yay" to words that begin with a vowel sound.

    "egg" → "eggyay"

 * 
 */

public class piglatin {

	private final static List<Character> vowels = Arrays.asList('a','e','i','o','u');
	
	public static String translate(String word) {
		String pigWord = "";
		
		/* Find the location of the first vowel
		 *     - FirstVowel is set to the location of first vowel character, stop the loop
		 *     - 'y' can be treated as a consonant or vowel depending on location *consonant at beginning of word
		 */
		
		int FirstVowel = 0;
		for(char charValue : word.toCharArray()) {
			if(isVowel(charValue) || (charValue == 'y' && FirstVowel > 0)){break;}
			FirstVowel++;
		}
		
		/* if the first character us a vowel, add way to the end, else take all leading consonants and push them to the back with ay at the end */
		if (FirstVowel == 0){
			pigWord = word + "way";
		} else {
			pigWord = word.substring(FirstVowel,word.length()) + word.substring(0,FirstVowel) + "ay";
		}
		
		return pigWord;
		
	}
	
	public static String translate(String[] words) {
		StringBuilder result = new StringBuilder();
		
		for(String word : words){
			if(word != null){			
				result.append(translate(word));
				result.append(" ");
			}
			
		}
		
		return result.toString();
		
	}
		
	private static Boolean isVowel(char value){
		return vowels.contains(Character.toLowerCase(value));
	}

}
