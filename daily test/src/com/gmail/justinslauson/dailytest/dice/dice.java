package com.gmail.justinslauson.dailytest.dice;

import java.util.Random;

public class dice {
	
	public static int[] Roll(int dice, int sides) {
		int[] results = new int[dice];
		Random randomGenerator = new Random();
		
		for (int diceroll = 1; diceroll < dice; ++diceroll){
			results[diceroll] = randomGenerator.nextInt(sides);
		}
		return results;
		
	}
}
